package com.example.cse438.blackjack.util

import android.content.Context
import android.view.View
import android.view.animation.Animation
import android.view.animation.ScaleAnimation
import android.view.animation.TranslateAnimation


class AnimationUtils {
    companion object {

        fun dpToPx(context: Context, dp: Int): Int {
            return (context.resources.displayMetrics.density * dp).toInt()
        }

        fun getViewTop(view: View): Int {
            var array = IntArray(2)
            view.getLocationInWindow(array)
            return array[1]
        }

        fun getAnimationHideToTop(marginTop: Float): Animation {
            return TranslateAnimation(TranslateAnimation.ABSOLUTE, 0F, TranslateAnimation.ABSOLUTE, 0F,
                    TranslateAnimation.RELATIVE_TO_SELF, 0F, TranslateAnimation.ABSOLUTE, -marginTop)
        }

        fun getAnimationShowFromTop(marginTop: Float): Animation {
            return TranslateAnimation(TranslateAnimation.ABSOLUTE, 0F, TranslateAnimation.ABSOLUTE, 0F,
                    TranslateAnimation.ABSOLUTE, -marginTop, TranslateAnimation.RELATIVE_TO_SELF, 0F)
        }


        fun getRotateHideAnimation(): Animation {
            return ScaleAnimation(1f, 0f, 1f, 1f,
                    ScaleAnimation.RELATIVE_TO_SELF, 0.5f, ScaleAnimation.RELATIVE_TO_SELF, 0.5f)
        }

        fun getRotateShowAnimation(): Animation {
            return ScaleAnimation(0f, 1f, 1f, 1f,
                    ScaleAnimation.RELATIVE_TO_SELF, 0.5f, ScaleAnimation.RELATIVE_TO_SELF, 0.5f)
        }

        fun cardFromTop(view: View, marginTop: Float, delay: Long, callback: View.OnClickListener?) {
            var hideAnimation = getAnimationHideToTop(marginTop)
            hideAnimation.duration = 0
            hideAnimation.fillAfter = true
            hideAnimation.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationEnd(animation: Animation?) {
                    var showAnimation =
                        getAnimationShowFromTop(marginTop)
                    showAnimation.duration = 500
                    showAnimation.fillAfter = true
                    showAnimation.setAnimationListener(object : Animation.AnimationListener {
                        override fun onAnimationEnd(animation: Animation?) {
                            callback?.onClick(view)
                        }

                        override fun onAnimationRepeat(animation: Animation?) {
                        }

                        override fun onAnimationStart(animation: Animation?) {
                            view.visibility = View.VISIBLE
                        }
                    })

                    if (delay > 0) {
                        view.postDelayed({ view.startAnimation(showAnimation) }, delay)
                    } else {
                        view.startAnimation(showAnimation)
                    }

                }

                override fun onAnimationRepeat(animation: Animation?) {

                }

                override fun onAnimationStart(animation: Animation?) {

                }
            })
            view.startAnimation(hideAnimation)
        }
    }

}