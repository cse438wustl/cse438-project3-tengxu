package com.example.cse438.blackjack

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), View.OnClickListener {
    override fun onClick(v: View) {
        val id = v.id
        when (id) {
            R.id.btRank -> startActivity(Intent(this@MainActivity,RankActivity::class.java))
            R.id.btPlay -> startActivity(Intent(this@MainActivity,GameActivity::class.java))
            R.id.logout -> startActivity(Intent(this@MainActivity,LoginActivity::class.java))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btRank.setOnClickListener(this)
        btPlay.setOnClickListener(this)
        logout.setOnClickListener(this)
    }


}