package com.example.cse438.blackjack

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import com.google.firebase.database.*
import com.google.firebase.quickstart.database.kotlin.models.User
import kotlinx.android.synthetic.main.activity_rank.*
import com.google.firebase.database.DataSnapshot



class RankActivity:AppCompatActivity(), View.OnClickListener{
    private val linearLayoutManager: LinearLayoutManager by lazy {
        LinearLayoutManager(this)
    }
    private var dataList: MutableList<User> = mutableListOf()
    private val mAdapter: RankAdapter by lazy {
        RankAdapter(this,dataList)
    }

    override fun onClick(v: View) {
        val id = v.id
        when (id) {
            R.id.back -> startActivity(Intent(this@RankActivity,MainActivity::class.java))

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rank)
        linearLayoutManager.let {
            it.orientation = LinearLayoutManager.VERTICAL
        }
        mRecyclerView.apply  {
            layoutManager = linearLayoutManager
            adapter = mAdapter
        }
        back.setOnClickListener(this@RankActivity)
        init()
    }

    private lateinit var database: DatabaseReference

    private fun init() {

        database = FirebaseDatabase.getInstance().getReference("users")
        database.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                Log.e("tag",""+dataSnapshot.childrenCount)
                Log.e("tag",""+dataSnapshot.toString())
                    for (data in dataSnapshot.children) {
                        Log.e("data",""+data.toString())
                        var user = data.getValue(User::class.java)
                        user?.let { dataList.add(it) }
                    }
                dataList.sortByDescending {
                    it.num
                }
                mAdapter.notifyDataSetChanged()
            }

            override fun onCancelled(p0: DatabaseError) {

            }

        })
    }
}