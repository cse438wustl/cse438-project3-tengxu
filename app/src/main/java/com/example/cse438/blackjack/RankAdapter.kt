package com.example.cse438.blackjack

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.google.firebase.quickstart.database.kotlin.models.User

class RankAdapter : RecyclerView.Adapter<RankAdapter.ViewHolder> {
    var mContext: Context
    var mDatas: List<User>
    var mInflater: LayoutInflater

    constructor(context: Context, list: List<User>) {
        this.mContext = context
        this.mDatas = list
        mInflater = LayoutInflater.from(mContext)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView: View = mInflater.inflate(R.layout.item_rank, parent,false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        if (mDatas.isNotEmpty() && mDatas != null) {
            return mDatas.size
        }
        return 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.name.text = mDatas[position].username
        holder.score.text = mDatas[position].num.toString()
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var name: TextView = itemView.findViewById(R.id.tv_name)
        var score: TextView = itemView.findViewById(R.id.tv_score)
    }
}