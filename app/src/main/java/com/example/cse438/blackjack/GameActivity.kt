package com.example.cse438.blackjack

import android.content.Intent
import android.graphics.drawable.Drawable
import android.media.SoundPool
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.view.GestureDetectorCompat
import android.support.v7.app.AlertDialog
import android.view.*
import android.view.animation.Animation
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import com.example.cse438.blackjack.util.CardRandomizer
import com.example.cse438.blackjack.util.AnimationUtils
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.google.firebase.quickstart.database.kotlin.models.User
import kotlinx.android.synthetic.main.activity_game.*
import kotlinx.android.synthetic.main.back_card_layout.view.*
import java.util.*

class GameActivity : AppCompatActivity() {
    private var marginPx :Int = 0
    private lateinit var userCards: ArrayList<Int>
    private lateinit var bankerCards: ArrayList<Int>
    private var randomizer = CardRandomizer()
    private var totalCard = 0
    private lateinit var cardList: ArrayList<Int>
    private lateinit var mDetector: GestureDetectorCompat
    private lateinit var mAuth: FirebaseAuth
    var num:Int = 0
    private lateinit var user: User
    var musicId = HashMap<Int, Int>()
    private var  soundPool= SoundPool(12, 0,5);
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)
        marginPx = AnimationUtils.dpToPx(this, 25)
        mAuth = FirebaseAuth.getInstance()
        mDetector = GestureDetectorCompat(this, MyGestureListener())
        musicId[1] = soundPool.load(this, R.raw.sound_win, 1)
        musicId[2] = soundPool.load(this, R.raw.sound_lost, 1)

        init()
        initView()
        if(mAuth.currentUser!=null){
            database = FirebaseDatabase.getInstance().getReference("users/"+mAuth.uid)
            database.addValueEventListener(object :ValueEventListener{
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    dataSnapshot.let {
                        user = dataSnapshot.getValue(User::class.java)!!
                        num = user.num!!
                        tvNum.text = "score:$num"
                    }

                }

                override fun onCancelled(p0: DatabaseError) {

                }

            })
        }

    }
    override fun onTouchEvent(event: MotionEvent) : Boolean {
        mDetector.onTouchEvent(event)
        return super.onTouchEvent(event)
    }
    private inner class MyGestureListener : GestureDetector.SimpleOnGestureListener() {

        private var swipeDistance = 150



        override fun onDoubleTap(e: MotionEvent?): Boolean {
            stand()
            return true
        }

        override fun onFling(e1: MotionEvent, e2: MotionEvent, velocityX: Float, velocityY: Float): Boolean {
            if (e2.x - e1.x > swipeDistance) {
                hit()
                return true
            }
            return false
        }
    }
    private lateinit var database: DatabaseReference
    private fun initView() {
        bt_shuffle.setOnClickListener {
            start()

        }
        menu.setOnClickListener{
            startActivity(Intent(this@GameActivity,MainActivity::class.java))
        }
    }

    //Start the game
    private fun stand() {
        //Check dealer's score
        val point = calculatePoint(bankerCards)
        if (point <= 16) {
            //if deal ger less than 16 will draw again
            val card = cardList.removeAt(0)
            bankerCards.add(card)
            //recursion part of add card
            addBankerCard(card, View.OnClickListener { stand() })
        } else {
            //flip part
            checkOverBankerCard(View.OnClickListener { checkWinner() })

        }
    }
    private fun checkWinner() {
        val bankerPoint = calculatePoint(bankerCards)
        val userPoint = calculatePoint(userCards)
        val msg = when {
            userPoint > 21 -> {
                num--
                soundPool.play(musicId[0]!!, 1F, 1F, 1, 0, 1F)
                "Player burst"

            }
            bankerPoint > 21 -> {
                num++
                soundPool.play(musicId[1]!!, 1F, 1F, 1, 0, 1F)
                "Dealer burst"

            }
            bankerPoint == userPoint -> {
                num--
                soundPool.play(musicId[0]!!, 1F, 1F, 1, 0, 1F)
                "Dealer reach 21, Dealer wins"

            }
            bankerPoint > userPoint -> {
                num--
                soundPool.play(musicId[0]!!, 1F, 1F, 1, 0, 1F)
                "Dealer win!!!"
            }
            else -> {
                num++
                soundPool.play(musicId[1]!!, 1F, 1F, 1, 0, 1F)
                "Player win!!!"

            }
        }
        val post = User( user.username, user.email, num)
        val postValues = post.toMap()
        database.updateChildren(postValues)
        AlertDialog.Builder(this)
                .setMessage(msg)
                .setTitle("result")
                .setPositiveButton("Keep Going") { _, _ ->
                    start()
                }
            .setNeutralButton("cancaled", null)
                .create()
                .show()

    }

    //Keep draw
    private fun hit() {
        val card = cardList.removeAt(0)
        userCards.add(card)
        addUserCard(card, View.OnClickListener { checkUserCards() })

    }

    //Check score
    private fun checkUserCards() {

        val point = calculatePoint(userCards)

       if (point > 21) {

           num--
           val post = User( user.username, user.email,num )
           val postValues = post.toMap()
           database.updateChildren(postValues)
           soundPool.play(R.raw.sound_lost, 1F, 1F, 1, 0, 1F)
           AlertDialog.Builder(this)
                   .setMessage("Player burst")
                   .setTitle("result")
                   .setPositiveButton("Keep going") { _, _ ->
                       start()
                   }
               .setNeutralButton("cancel", null)
                   .create()
                   .show()
        }

    }

    private fun calculatePoint(cards: ArrayList<Int>): Int {
        var count = 0
        var result = 0
        for (card in cards) {
            val point = randomizer.getPoint(card)
            result += point
            if (point == 1) {
                count++
            }
        }

        if (result <= 11 && count > 0) {
            result += 10
        }
        return result
    }
    private fun init() {
        cardList = randomizer.getWashedCards()
        totalCard = cardList.size
        bankerCards = arrayListOf()
        userCards = arrayListOf()

    }


    private fun start() {
        if (cardList.size < totalCard / 2) {
            cardList = randomizer.getWashedCards()
        }
        var blackJackList = if (Random().nextInt(20) == 1) randomizer.blackJack(cardList) else null

        userCards = blackJackList ?: arrayListOf(cardList.removeAt(0), cardList.removeAt(0))//arrayListOf(1, 10)//
        bankerCards = arrayListOf(cardList.removeAt(0), cardList.removeAt(0))
        showUserCard(userCards)
        showBankerCard(bankerCards)
        checkBankerCardis21()
    }

    private fun checkBankerCardis21() {


        if(calculatePoint(bankerCards)==21&&bankerCards.size==2){

            checkOverBankerCard(object :View.OnClickListener{
                override fun onClick(p0: View?) {
                    num--
                    val post = User( user.username, user.email,num)
                    val postValues = post.toMap()
                    database.updateChildren(postValues)
                    AlertDialog.Builder(this@GameActivity)
                            .setMessage("Dealer wins")
                            .setTitle("result")
                            .setPositiveButton("Keep going") { _, _ ->
                                start()
                            }
                        .setNeutralButton("canceled", null)
                            .create()
                            .show()

                }

            })

        }
    }
    private fun checkOverBankerCard(callback: View.OnClickListener) {
        if (bankerCards.size>=2){
            val view: View = bankerCardsLayout.getChildAt(1)
            turnCard(view, R.id.backView, R.id.frontView,callback)
        }

    }
    private fun turnCard(cardRootView: View, backId: Int, frontId: Int, callbacks: View.OnClickListener) {
        val hideAnimation = AnimationUtils.getRotateHideAnimation()
        hideAnimation.duration = 500
        hideAnimation.fillAfter = true

        val showAnimation = AnimationUtils.getRotateShowAnimation()
        showAnimation.duration = 500
        showAnimation.fillAfter = true

        val frontView:ImageView = cardRootView.findViewById(frontId)

        hideAnimation.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationEnd(animation: Animation?) {
                frontView.startAnimation(showAnimation)
                callbacks.onClick(cardRootView)
            }

            override fun onAnimationRepeat(animation: Animation?) {

            }

            override fun onAnimationStart(animation: Animation?) {
                val hideAnimation = AnimationUtils.getRotateHideAnimation()
                hideAnimation.duration = 0
                hideAnimation.fillAfter = true
                frontView.startAnimation(hideAnimation)
            }
        })
        val frontImage :ImageView  = cardRootView.findViewById(backId)
        frontImage  .startAnimation(hideAnimation)
    }


    private fun showBankerCard(bankerCards: ArrayList<Int>) {
        setBankerCard(bankerCards)
    }

    private fun showUserCard(array: ArrayList<Int>) {
        setUserCard(userCardsLayout, array)
    }

    private fun setUserCard(viewGroup: ViewGroup, cards: ArrayList<Int>) {
        viewGroup.removeAllViews()
        for (i in 0 until cards.size) {
            var imageView = ImageView(this)
            var drawable = getDrawable(randomizer.getCardRes(this,cards[i]))
            imageView.setImageDrawable(drawable)
            if (cards.size > 1) {
                imageView.visibility = View.GONE
            }

            val layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            layoutParams.gravity = Gravity.CENTER
            layoutParams.leftMargin = if (i == 0) 0 else -(drawable.intrinsicWidth - marginPx)
            viewGroup.addView(imageView, layoutParams)

           if (cards.size > 1) {
                AnimationUtils.cardFromTop(imageView, AnimationUtils.getViewTop(viewGroup).toFloat(), if (i == cards.size - 1) (if (cards.size == 1) 500L else 60) else 0, null)
            }
        }
    }
    private fun addBankerCard(card: Int,callback: View.OnClickListener) {
        addCard(card, bankerCardsLayout,callback)
    }
    private fun addUserCard(card: Int,callback: View.OnClickListener) {
        addCard(card, userCardsLayout,callback)
    }
    private fun addCard(card: Int, viewGroup: ViewGroup,callback: View.OnClickListener) {
        var imageView = ImageView(this)
        var drawable = getDrawable(randomizer.getCardRes(this,card))
        imageView.setImageDrawable(drawable)
        imageView.visibility = View.GONE

        val layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        layoutParams.leftMargin = -(drawable.intrinsicWidth - marginPx)
        viewGroup.addView(imageView, layoutParams)
        AnimationUtils.cardFromTop(imageView, AnimationUtils.getViewTop(viewGroup).toFloat(), 0L, callback )
    }
    private fun setBankerCard(cards: ArrayList<Int>) {
        bankerCardsLayout.removeAllViews()
        var blackCard = resources.getDrawable(R.drawable.back, theme)
        for (i in 0 until cards.size) {
            var itemView: View
            var drawable: Drawable = getDrawable(randomizer.getCardRes(this,cards[i]))
            itemView = if (i != 0) {
                var view = View.inflate(this, R.layout.back_card_layout, null)
                view.backView.setImageDrawable(blackCard)
                view.frontView.setImageDrawable(drawable)
                view
            } else {
                var imageView = ImageView(this)
                imageView.setImageDrawable(drawable)
                imageView
            }

            itemView.visibility = View.GONE
            val layoutParams = FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT)
            layoutParams.leftMargin = if (i == 0) 0 else -(drawable.intrinsicWidth - marginPx)

            bankerCardsLayout.addView(itemView, layoutParams)
            if (i == 0) {
                AnimationUtils.cardFromTop(itemView, AnimationUtils.getViewTop(bankerCardsLayout).toFloat(), 0L, null)
            } else {
                AnimationUtils.cardFromTop(itemView, AnimationUtils.getViewTop(bankerCardsLayout).toFloat(), 60L, null)
            }
        }
    }




}
